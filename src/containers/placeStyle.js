const placeStyle = {
    box: {
        width: '100%',
        height: 'auto',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        margin: 10,
        padding: 10,
        elevation: 10,
        backgroundColor: 'rgb(240, 240, 240)',
    },
    itemImage: {
        width: 50,
        height: 50,
        flex: 0,
        marginRight: 5,
    },
    itemText: {
        width: '88%',
    }
}

export default placeStyle;