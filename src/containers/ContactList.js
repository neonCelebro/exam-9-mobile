import React from 'react';
import {connect} from 'react-redux';
import {FlatList, Image, Text, TouchableHighlight, View} from "react-native";

import placeStyle from './placeStyle';
import {fetchContacts} from "../store/contacts/actions";


class ContactList extends React.Component {

    componentDidMount() {
        this.props.initialContacts();
    };

    render() {
        return (
            <View>
                <FlatList
                    data={Object.values(this.props.contacts)}
                    keyExtractor={(item, index) => index}
                    renderItem={({item}) => {
                        return (
                            <TouchableHighlight style={placeStyle.box}>
                                <View>
                                    <Image style={placeStyle.itemImage} source={{uri: item.photo}}/>
                                    <Text style={placeStyle.itemText}>{item.name}</Text>
                                </View>
                            </TouchableHighlight>
                        )
                    }}
                />
            </View>

        )
    }
}


const mapStoreToProps = state => {
    return {
        contacts: state.contacts,
    }
};
const mapDispatchToProps = dispatch => {
    return {
        initialContacts: () => dispatch(fetchContacts()),
    }
};

export default connect(mapStoreToProps, mapDispatchToProps)(ContactList);