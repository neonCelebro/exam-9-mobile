import * as actions from './actions';

const initialState = {
    contacts: {},
    isLoading: false,
    error: false,
    editStatus: false,
    edited: {},
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actions.FETCH_REQUEST:
            return {...state, isLoading: true};
        case actions.FETCH_ERROR:
            return {...state, error: true};
        case actions.FETCH_MENU_SUCCESS:
            return {...state, contacts: action.data};
        case actions.EDIT_CONTACT:
            return{...state, edited: action.data, editStatus: true};
        case actions.STOP_EDIT_CONTACT:
            return{...state, editStatus: false};

        default:
            return state;
    }
};

export default reducer;
