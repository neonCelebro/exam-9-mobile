import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://telephonebook-b6762.firebaseio.com/'
});


export default instance;